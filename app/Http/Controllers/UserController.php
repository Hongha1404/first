<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    function __construct()
    {
        if (Auth::check()) {
            $users = Auth::user();
            view()->share('users', $users);
        }
    }

    public function check(Request $request)
    {

        $data=[
            'email'=>$request->email,
            'password'=>$request->password,
        ];
        if(Auth::attempt($data)){
            //true
            return redirect('home');
        }else{
            //false
            return redirect('login');
        }
    }
    public function getRegister()
    {
        return view('register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required|min:3|max:50',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6|max:32',
                'password_again' => 'required|same:password'
            ],
            [
                'username.required' => 'Bạn chưa nhập Tên tài khoản!',
                'username.min' => 'Tên tài khoản gồm tối thiểu 3 ký tự!',
                'username.max' => 'Tên tài khoản không được vượt quá 50 ký tự!',
                'email.required' => 'Bạn chưa nhập địa chỉ Email!',
                'email.email' => 'Bạn chưa nhập đúng định dạng Email!',
                'email.unique' => 'Địa chỉ Email đã tồn tại!',
                'password.required' => 'Bạn chưa nhập mật khẩu!',
                'password.min' => 'Mật khẩu gồm tối thiểu 6 ký tự!',
                'password.max' => 'Mật khẩu không được vượt quá 32 ký tự!',
                'password_again.required' => 'Bạn chưa xác nhận mật khẩu!',
                'password_again.same' => 'Mật khẩu xác nhận chưa khớp với mật khẩu đã nhập!'
            ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password_confirmation);


        $user->save();
        return redirect('login')->with('message', 'Đăng ký tài khoản thành công!');
    }
    public function logoutFb(){
        Auth::logout();
        return redirect('home');
    }
}
