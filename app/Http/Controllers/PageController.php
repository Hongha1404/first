<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function search(Request $request)
    {
        $key = $request->key;
        $article = Article::where('title', 'like', "%$key%")->paginate(5);

        return view('page.search', ['article' => $article, 'key' => $key]);
    }
    public function index(){
        return view('auth.FbLogin.Facebook.fb-callback.php');
    }
}
