<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(){
        $category = Category::all();
        view()->share('category', $category);
    }
    public function sidebar(){
        $category = Category::all();
        return view('layout.sidebar', ['category'=> $category]);
    }
    public function getChoose(){
        return view('layout.sidebar');
    }
    public function postChoose(){
//        $category = Category::all();
//        return view('layout.sidebar', ['category'=> $category]);
////        return view('home');
    }
}
