<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Category;

class ArticlesController extends Controller
{
    //
    /**
     * @return string
     */
    public function __construct(){
        $category = Category::all();
        view()->share('category', $category);
    }
    public function index(Request $request)
    {
        // phân trang với 5 record , khi scroll late, thì 5 record tiếp theo sẽ được load
        $articles = Article::paginate(5);
        $category = Category::all();
        return view('article.index',compact('articles'), ['category'=> $category]);
//        return view('article.index', compact("articles"));
    }
    public function search(Request $request)
    {
        $key = $request->key;
        $articles = Article::where('title', 'like', "%$key%")->paginate(5);

        return view('page.search', ['article' => $articles, 'key' => $key]);
    }
}