<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // xác định các cột có thể được thay đổi bời người dùng
    protected $fillable = [
        'title',
        'content',
    ];

    // đăt tên table
    protected $table="articles";
}
