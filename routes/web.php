<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('home', 'ArticlesController@index');
Route::get('login', 'UserController@check');
Route::get('logoutFb', 'UserController@logoutFb');
Route::get('register', 'UserController@getRegister');
Route::post('register', 'UserController@postRegister');
Route::get('choose', 'CategoryController@getChoose');
Route::post('choose', 'CategoryController@postChoose');
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('search', 'ArticlesController@search');
Route::get('fb-callback', 'PageController@index');


//LOGIN FB
//Route::get('/auth/facebook', 'SocialAuthController@redirectToProvider');
//Route::get('/auth/facebook/callback', 'SocialAuthController@handleProviderCallback');

//Route::get('auth/{provider}', 'SocialAuthController@redirectToProvider');
//Route::get('auth/{provider}/callback', 'SocialAuthController@handleProviderCallback');


//Route::get('auth/facebook', 'Auth\LoginController@redirectToProvider');
//Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/redirect/facebook', 'SocialAuthController@redirectToProvider');
Route::get('/callback/facebook', 'SocialAuthController@handleProviderCallback');