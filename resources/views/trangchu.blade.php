@extends('layouts.index')

@section('content')

  <div class="col-md-6" id="page-content">
    <!--Carousel-->
    @include('layouts.carousel')
    <!--End Carousel-->

    <!-- Post -->
    <div class="post-group">
      <div class="post-header">
        <a href="#"><img src="{{ asset('img/avatar.jpg')}}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
        <div class="post-info">
          <a href="#">Đặng Yến</a>
          <h6>1 giờ trước</h6>
        </div>
      </div>
      <div class="post-content">
        <p>Rạng sáng 11/7, xe khách giường nằm va chạm container rồi bốc cháy ngùn ngụt trên đường vành đai 3 trên cao, đoạn gần ngã tư Phạm Hùng - Khuất Duy Tiến, hướng về Mai Dịch, Hà Nội. Ngay sau đó xe giường nằm bị thiêu rụi hoàn toàn, trơ khung sắt; phần đuôi bên phải bánh romooc bị nổ lốp và cháy rụi ... <a href="#">Xem thêm</a></p>
        <img src="{{ asset('img/post-img.jpg') }}" alt="hình ảnh" width="100%">
      </div>
      <div class="post-action">
        <ul class="list-inline">
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-heart"></span></a>25 Yêu thích</li>
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-comment"></span></a>5 Bình luận</li>
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-share-alt"></span></a>2 Chia sẻ</li>
        </ul>
      </div>
      <div class="post-comment">
        <div class="post-comment-info">
          <div class="comment-item">
            <a href="#"><img src="{{ asset('img/avatar.jpg') }}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
            <div class="comment-item-info">
              <a class="comment-owner" href="#">Đặng Yến</a>
              <p>Bây giờ thế nào rồi. Không biết bây giờ thế nào rồi. Thế nào rồi nhỉ? Mọi chuyện rồi cũng sẽ qua thôi :(</p>
              <a href="#">Thích</a>
              <a href="#">Trả lời</a>
              <a class="comment-time">Vài phút trước</a>
            </ul>
            </div>
          </div>
          <div class="comment-item">
            <a href="#"><img src="{{ asset('img/avatar.jpg') }}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
            <div class="comment-item-info">
              <a class="comment-owner" href="#">Đặng Yến</a>
              <p>Bây giờ thế nào rồi.</p>
              <a href="#">Thích</a>
              <a href="#">Trả lời</a>
              <a class="comment-time">Vài phút trước</a>
            </ul>
            </div>
          </div>
          <a href="#">Xem thêm bình luận</a>
        </div>
        <div class="post-comment-form">
          <form>
            <textarea rows="1" class="form-control" name="" placeholder="Viết bình luận..."></textarea>
          </form>
        </div>
      </div>
    </div>
    <!--End Post-->

    <!-- Post -->
    <div class="post-group">
      <div class="post-header">
        <a href="#"><img src="{{ asset('img/avatar.jpg') }}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
        <div class="post-info">
          <a href="#">Đặng Yến</a>
          <h6>1 giờ trước</h6>
        </div>
      </div>
      <div class="post-content">
        <p>Rạng sáng 11/7, xe khách giường nằm va chạm container rồi bốc cháy ngùn ngụt trên đường vành đai 3 trên cao, đoạn gần ngã tư Phạm Hùng - Khuất Duy Tiến, hướng về Mai Dịch, Hà Nội. Ngay sau đó xe giường nằm bị thiêu rụi hoàn toàn, trơ khung sắt; phần đuôi bên phải bánh romooc bị nổ lốp và cháy rụi ... <a href="#">Xem thêm</a></p>
        <img src="img/post-img.jpg" alt="hình ảnh" width="100%">
      </div>
      <div class="post-action">
        <ul class="list-inline">
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-heart"></span></a>25 Yêu thích</li>
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-comment"></span></a>5 Bình luận</li>
            <li class="post-reaction"><a href="#"><span class="glyphicon glyphicon-share-alt"></span></a>2 Chia sẻ</li>
        </ul>
      </div>
      <div class="post-comment">
        <div class="post-comment-info">
          <div class="comment-item">
            <a href="#"><img src="{{ asset('img/avatar.jpg') }}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
            <div class="comment-item-info">
              <a class="comment-owner" href="#">Đặng Yến</a>
              <p>Bây giờ thế nào rồi. Không biết bây giờ thế nào rồi. Thế nào rồi nhỉ? Mọi chuyện rồi cũng sẽ qua thôi :(</p>
              <a href="#">Thích</a>
              <a href="#">Trả lời</a>
              <a class="comment-time">Vài phút trước</a>
            </ul>
            </div>
          </div>
          <div class="comment-item">
            <a href="#"><img src="{{ asset('img/avatar.jpg') }}" alt="avatar" class="img-circle" id="avatar" width="40px" height="40px"></a>
            <div class="comment-item-info">
              <a class="comment-owner" href="#">Đặng Yến</a>
              <p>Bây giờ thế nào rồi.</p>
              <a href="#">Thích</a>
              <a href="#">Trả lời</a>
              <a class="comment-time">Vài phút trước</a>
            </ul>
            </div>
          </div>
          <a href="#">Xem thêm bình luận</a>
        </div>
        <div class="post-comment-form">
          <form>
            <textarea rows="1" class="form-control" name="" placeholder="Viết bình luận..."></textarea>
          </form>
        </div>
      </div>
    </div>
    <!--End Post-->
  </div>

@endsection