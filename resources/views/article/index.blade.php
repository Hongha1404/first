@extends('layouts.index')
@section('content')
<body>
<div id="content">
    <div class="row">
        <div class="post-content">
            <h1 >Article List</h1>
            <hr>
            <div class="infinite-scroll">

                @foreach($articles as $article)
                    <div class="post-item">
                        <div class="username">
                            <a href="#"><img src={{asset('images/TVXQ.png')}} class="avatar"></a>
                            <a href="#"><h5>user</h5></a>
                        </div>
                        <h3><span>Title: {{$article->title}}</span></h3>
                        <p>{{$article->content}}</p>
                        <hr>
                        <div class="like">
                            <a href="#"><img src={{ asset('images/icon-like.png') }} class="like"></a><a href="#">Like</a>
                            <a href="#"><img src={{asset('images/comment.png')}} class="like"></a><a href="#">Comment</a>
                            <a href="#"><img src={{asset('images/share.png')}} class="like"></a><a href="#">Share</a>
                            <div class="comment"><input type="text" class="comment" placeholder = "Viết bình luận"/></div>
                        </div>
                    </div>
                @endforeach

                {{ $articles->links() }}
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    // ẩn thanh phân trang của laravel
    $('ul.pagination').hide();

    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<center><img src={{asset('images/loading.gif')}} class="center-block" alt="Loading..."></center>',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                // xóa thanh phân trang ra khỏi html mỗi khi load xong nội dung
                $('ul.pagination').remove();
            }
        });
    });
</script>
</body>
@endsection