<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <div class="item active">
      <img src="{{ asset ('img/slide1.jpg') }}" alt="Los Angeles">
      <div class="carousel-caption">
        <h3 class="category-carousel">Sport</h3>
        <p class="title-carousel">World Cup 2018</p>
      </div>
    </div>

    <div class="item">
      <img src="{{ asset ('img/slide4.jpg') }}" alt="Chicago">
      <div class="carousel-caption">
        <h3 class="category-carousel">Chicago</h3>
        <p class="title-carousel">Thank you, Chicago!</p>
      </div>
    </div>
  
    <div class="item">
      <img src="{{ asset ('img/slide5.jpg') }}" alt="New York">
      <div class="carousel-caption">
        <h3 class="category-carousel">New York</h3>
        <p class="title-carousel">We love the Big Apple!</p>
      </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>