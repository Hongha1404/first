<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="{{ asset ('img/logo.png') }}" alt="hình ảnh" height="40px"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" class="page-header">
      <ul class="nav navbar-nav">
        <li><a href="#">Trang chủ</a></li>
        <li><a href="#">Khám phá</a></li>
      </ul>
      <form class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" class="form-control" id="search-text" placeholder="Tìm kiếm...">
        </div>
        <button type="submit" class="btn btn-info" id="search-submit"><span class="glyphicon glyphicon-search"></span></button>
      </form>
      <ul class="nav navbar-nav navbar-right">          
        <li class="login-fb"><a href="#">Đăng nhập Facebook</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>