<!DOCTYPE html>
<html>
<head>
	<title>News Feed</title>
	<meta charset=utf-8>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
  <!-- Navigation Bar -->
  @include('layouts.header')
  <!-- End Navigation Bar -->
	
  <div class="container">
    <div class="row">
      <!--Left sidebar-->
      @include('layouts.left-sidebar')
      <!--/span-->
      
      @yield('content')

      <!--/right sidebar-->
      @include('layouts.right-sidebar')
      <!--/span-->
    </div>
    <!--/row-->
  </div>

	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <!-- <script src="js/fixed-header.js"></script> -->
</body>
</html>