<?php 
//require_once "config.php";
require_once "Facebook/autoload.php";
$fb = new Facebook\Facebook([
    'app_id' => '961266800720196', // Replace {app-id} with your app id
    'app_secret' => '271a7c572295d170a4f1e038453c793b',
    'default_graph_version' => 'v2.2',
]);

$helper = $fb->getRedirectLoginHelper();

try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit();
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit();
}

if (! isset($accessToken)) {
    header('Location: login.blade.php');
  exit();
}
$oAuth2Client = $fb->getOAuth2Client();

if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } 

$respond = $fb->get("me?fields=id,name,email", $accessToken);
$userdata = $respond->getGraphNode()->asArray();
$_SESSION['userData'] = $userdata;
$_SESSION['access_token'] = (string) $accessToken;

echo $accessToken;
header('Location: index.blade.php');
exit();

// User is logged in with a long-lived access token.
// You can redirect them to a members-only page.
//header('Location: https://example.com/members.php');
?>