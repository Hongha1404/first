@extends('layout.index')

@section('content')

    <div class="post-content">
        <?php
        function change($str, $key){
            return str_replace($key, "<span style='color:red;'>$key</span>", $str);
        }
        ?>
        <div class="container_12">
            <div class="grid_9">

                <h3>Tìm kiếm: từ khóa <span style="color: #00b3ee">{{$key}}</span> có {{count($article)}} kết quả</h3>

                @foreach ($article as $a)
                    <div class="tab_cont" id="tabs-2">
                        {{--<img src="upload/tintuc/{{$article['Hinh']}}" alt="">--}}
                        <div class="extra_wrapper">
                            <div class="text1"><h3>Title: {!! change($a->title, $key) !!}{{$a['title']}}</h3> </div>
                            <p class="style1"> {{$a['content']}} </p>

                            <div class="clear "></div>

                        </div>
                        <div class="clear cl1"></div>
                        <div class="clear cl1"></div>

                    </div>

                @endforeach

            </div>

            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
@endsection