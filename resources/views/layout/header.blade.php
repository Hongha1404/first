    <div id="header">
        <nav id="fixNav">


            <ul>
                <a href="trangchu"><li><img src={{asset('images/TVXQ.png')}} id="logo"></li></a>
                <a href="trangchu"><li>Trang chủ</li></a>
                <a href=""><li>Video</li></a>
                <a href=""><li>Cộng đồng</li></a>
            </ul>
            <form action="search.html" method="post">
                <input type="submit" name="btn_login" class="search-form" id="btn_search" value="Tìm kiếm">
                <input type="text" name="txt_search" class="search-form" id="txt_search" placeholder="Nhập nội dung...">

            </form>

            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                    {{--</li>--}}
                    <div id="login">
                        <a href="login">Đăng nhập</a>
                        <a href="register">Đăng ký</a>
                    </div>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                            <a class="dropdown-item" href="logout"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="logout" method="POST" style="display: none;">
                                @csrf
                            </form>

                    </li>

                @endguest
            </ul>
        </nav>
    </div>