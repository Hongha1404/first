<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <title>Social network</title>
    {{--app--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    {{--app--}}
    <link type="text/css" href={{url("css/style2.css")}} rel="stylesheet">
    <link type="text/css" href={{url("css/style.css")}} rel="stylesheet">
    <link rel="stylesheet" href={{url("bootstrap/css/bootstrap.min.css")}}}>
    <script src={{url("bootstrap/js/bootstrap.min.js")}}></script>
    <script src={{url("https://code.jquery.com/jquery-3.2.1.min.js")}}></script>
    <script src={{url("https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.7/jquery.jscroll.js")}}></script>
    <!-- Scripts -->
    {{--<script src="{{url('js/app.js') }}" defer></script>--}}
</head>

<body>
<!--FIX menu-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=201254217204503&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : '201254217204503',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v3.0'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="container">
    <div id="wrapper">
        @yield('header')
        @include('layout.sidebar')
        @include('layout.header')
        @yield('content')
    </div>
</body>
</html>